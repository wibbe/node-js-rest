require('dotenv').config()


const express = require("express");
const bodyParser = require("body-parser");
const app = express();

//the api routes for the User endpoint
const User = require("./routes/user");

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);



//a quick way to host the angularjs, better would be to use a seperate container running it with nginx or similar
app.use(express.static(__dirname + '/app')); //Serves resources from public folder

//valid endpoints
app.get("/api/users", User.getUsers);
app.post("/api/users", User.createUser);
//should have other endpoints in a real world case
//getUser/$id GET user by id
//deleteUser/$id DELETE user by id
//updateUser/$id PUT user data
//searchUsers/{params} GET search user by "email=ki*" etc. OR as a POST with key value params

app.listen(3319, () => {
  console.log("App running on port 3319.");
});