//DB part
const Pool = require("pg").Pool;
const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
});

//validation part
const Joi = require("@hapi/joi");
const userSchema = Joi.object().keys({
  username: Joi.string()
    .min(3)
    .max(30)
    .required(),
  email: Joi.string()
    .email()
    .required()
});

//db interaction, could have been in a orm like sequelize if project was bigger!
const getUsers = (request, response) => {
  pool.query("SELECT * FROM demousers ORDER BY id ASC", (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

const createUser = (request, response) => {
  const { username, email } = request.body;
  const { error, value } = userSchema.validate({
    username: username,
    email: email
  });

  if (error) {
    response.status(422).json({ errors: error.details });
  } else {
    pool.query(
      "INSERT INTO demousers (username, email) VALUES ($1, $2)",
      [username, email],
      (error, result) => {
        if (error) {
          response.status(422).json({ errors: [{ message: error.detail }] });
        } else {
          response.status(201).send("User added");
        }
      }
    );
  }
};

module.exports = {
  getUsers,
  createUser
};
