#!/bin/bash

#ADD TO CRON, example run it every night at 0200 as root
#
# -->0 2 * * *   root    /PATHTOFILE/backup.sh
#

# Location to place backups.
#create dir if not exists
mkdir -p ~/backups/databases/latest
backup_dir=~/backups/databases/
nightly_dir=~/backups/databases/latest/

#String to append to the name of the backup files
backup_date=`date +%d-%m-%Y`

#Numbers of days you want to keep copy of your databases
number_of_days=15

databases=`psql -l -t | cut -d'|' -f1 | sed -e 's/ //g' -e '/^$/d'`
for i in $databases; do  if [ "$i" != "postgres" ] && [ "$i" != "template0" ] && [ "$i" != "template1" ] && [ "$i" != "template_postgis" ]; then    
    echo Dumping $i to $backup_dir$i\_$backup_date.sql    
    pg_dump $i > $backup_dir$i\_$backup_date.sql
    bzip2 $backup_dir$i\_$backup_date.sql
    ln -fs $backup_dir$i\_$backup_date.sql.bz2 $nightly_dir$i-nightly.sql.bz2
  fi
done
find $backup_dir -type f -prune -mtime +$number_of_days -exec rm -f {} \;