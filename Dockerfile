FROM node:10
WORKDIR /dockerApp
COPY package.json /dockerApp
RUN npm install
COPY . /dockerApp
CMD node index.js
EXPOSE 3319