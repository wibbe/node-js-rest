"use strict";

angular
    .module("demoApp.listusers", ["ngRoute"])

    .config([
        "$routeProvider",
        function ($routeProvider) {
            $routeProvider.when("/listusers", {
                templateUrl: "listusers/listusers.html",
                controller: "ListUsersController"
            });
        }
    ])

    .controller('ListUsersController', function ListUsersController($scope, $http) {
        const apiSource = "/api/users";
        $http
            .get(apiSource)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.users = response.data;
                }
            }, function (error) {
                console.log(error)
            });

    })
