"use strict";

angular
    .module("demoApp.register", ["ngRoute"])

    .config([
        "$routeProvider",
        function ($routeProvider) {
            $routeProvider.when("/register", {
                templateUrl: "register/register.html",
                controller: "RegisterController"
            });
        }
    ])

    .controller('RegisterController', function RegisterController($scope, $http) {
        const apiSource = "/api/users";

        $scope.user = {};


        $scope.submitForm = function () {
            $http
                .post(apiSource, $scope.user)
                .then(function (response) {
                    if (response.status === 201) {
                        //all ok
                        $scope.postSuccess = 'ok';
                    }
                }, function (error) {
                    $scope.errorName = error.data.errors[0].message;
                    console.log($scope.errorName);
                });
        };

        $scope.clearInputs = function () {
            $scope.postSuccess = null;
            $scope.errorName = null;
        }
    })
