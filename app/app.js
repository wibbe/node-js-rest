// // Define the `demoApp` module
// var demoApp = angular.module('demoApp', ['ngRoute']);

// // Define the `DemoAppController` controller
// demoApp
//     .controller('DemoAppController', function DemoAppController($scope, $http) {

//         const apiSource = "http://localhost:3319/api/users";

//         $http
//             .get(apiSource)
//             .then(function (response) {
//                 if (response.status === 200) {
//                     $scope.users = response.data
//                 }
//             }, function (error) {
//                 console.log(error)
//             });

//     })
//     .config([''])


"use strict";

// Declare app level module which depends on views, and core components
angular
    .module("demoApp", [
        "ngRoute",
        "demoApp.register",
        "demoApp.listusers",
    ])
    .config([
        "$locationProvider",
        "$routeProvider",
        function ($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix("!");
            $routeProvider.otherwise({ redirectTo: "/register" });
        }
    ]);
